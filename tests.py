from selenium import webdriver
import time

def main():
    driver = webdriver.Chrome()
    driver.get('https://i2crm.ru/')
    btm_lm = driver.find_element_by_id('uid60')
    btm_lm.click()

    element_1 = driver.find_element_by_id('signup-name')
    element_1.send_keys("Test")

    element_2 = driver.find_element_by_id('signup-email')
    element_2.send_keys("sh.merlok@yandex.ru")

    element_3 = driver.find_element_by_id('signup-phone_national')
    element_3.send_keys("89969075832")

    btm_lm2 = driver.find_element_by_name('signup-button')
    btm_lm2.click()

    time.sleep(10)

    driver.quit()

if __name__ == "__main__":
    main()
